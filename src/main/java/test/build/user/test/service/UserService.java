package test.build.user.test.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import test.build.user.test.dto.request.UserRequest;
import test.build.user.test.dto.response.UserResponse;
import test.build.user.test.entity.User;
import test.build.user.test.exception.WrongInputDataException;
import test.build.user.test.repository.UserRepository;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public UserResponse save(UserRequest userRequest) throws IOException {
        User user = new User();
        user.setUsername(userRequest.getUsername());
        user.setPassword(userRequest.getPassword());
        User save = userRepository.save(user);
        return new UserResponse(save);
    }

    public List<UserResponse> findAll() {
        return userRepository.findAll().stream().map(UserResponse::new).collect(Collectors.toList());
    }

    public User findOne(Long id) {
        return userRepository.findById(id).orElseThrow(() -> new WrongInputDataException("User with id " + id + " not exist"));
    }

    public void delete(Long id) {
        userRepository.delete(findOne(id));
    }
}
