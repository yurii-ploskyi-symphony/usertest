package test.build.user.test.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import test.build.user.test.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
}
