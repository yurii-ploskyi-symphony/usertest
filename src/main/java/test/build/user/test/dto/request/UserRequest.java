package test.build.user.test.dto.request;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Getter
@Setter
public class UserRequest {

    @NotBlank
    @Size(min = 4, max = 25)
    private String username;
    @NotBlank
    private String password;
}
