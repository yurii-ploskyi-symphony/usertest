package test.build.user.test.dto.response;

import lombok.Getter;
import lombok.Setter;
import test.build.user.test.entity.User;

@Getter
@Setter
public class UserResponse {

    private Long id;
    private String username;
    private String password;

    public UserResponse(User user) {
        id = user.getId();
        username = user.getUsername();
        password = user.getPassword();
    }
}
